package net.theprogrammersworld.herobrine.listeners;

import java.util.Random;
import java.util.logging.Logger;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.misc.ItemName;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;

public class InventoryListener implements Listener {
	Logger log;

	public InventoryListener() {
		super();
		log = Logger.getLogger("Minecraft");
	}

	@EventHandler
	public void onInventoryClose(final InventoryCloseEvent event) {
		if (event.getInventory().getType() == InventoryType.CHEST) {
			final Object[] data = { event.getPlayer(), event.getInventory() };
			Herobrine.getPluginCore().getAICore().getCore(Core.CoreType.BOOK).runCore(data);
			if ((new Random().nextInt(100) > 97) && Herobrine.getPluginCore().getConfigDB().UseHeads && (event.getInventory().firstEmpty() != -1)
					&& Herobrine.getPluginCore().getAICore().getResetLimits().isHead()) {
				event.getInventory().setItem(event.getInventory().firstEmpty(), ItemName.CreateSkull(event.getPlayer().getName()));
			}
		}
	}

	@EventHandler
	public void onInventoryOpen(final InventoryOpenEvent event) {
		if (((event.getInventory().getType() == InventoryType.CHEST) || (event.getInventory().getType() == InventoryType.FURNACE) || (event.getInventory().getType() == InventoryType.WORKBENCH))
				&& Herobrine.getPluginCore().getConfigDB().useWorlds.contains(event.getPlayer().getLocation().getWorld().getName()) && Herobrine.getPluginCore().getConfigDB().PlaceSigns
				&& Herobrine.getPluginCore().getSupport().checkSigns(event.getPlayer().getLocation()) && Herobrine.getPluginCore().getAICore().getResetLimits().isSign()) {
			final Object[] data = { event.getPlayer().getLocation(), event.getPlayer().getLocation() };
			Herobrine.getPluginCore().getAICore().getCore(Core.CoreType.SIGNS).runCore(data);
		}
	}
}
