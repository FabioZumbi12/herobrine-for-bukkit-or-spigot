package net.theprogrammersworld.herobrine.listeners;

import java.util.ArrayList;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.AICore;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.cores.Heads;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockListener implements Listener {

	@EventHandler
	public void onBlockIgnite(final BlockIgniteEvent event) {
		if (event.getBlock() != null) {
			final Block blockt = event.getBlock();
			final Location blockloc = blockt.getLocation();
			if (event.getPlayer() != null) {
				blockloc.setY(blockloc.getY() - 1.0);
				final Block block = blockloc.getWorld().getBlockAt(blockloc);
				if ((block.getType() == Material.NETHERRACK) && (block.getWorld().getBlockAt(blockloc.getBlockX(), blockloc.getBlockY() - 1, blockloc.getBlockZ()).getType() == Material.NETHERRACK)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX() - 1, blockloc.getBlockY() - 1, blockloc.getBlockZ()).getType() == Material.GOLD_BLOCK)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX() - 1, blockloc.getBlockY() - 1, blockloc.getBlockZ() - 1).getType() == Material.GOLD_BLOCK)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX() - 1, blockloc.getBlockY() - 1, blockloc.getBlockZ() + 1).getType() == Material.GOLD_BLOCK)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX() + 1, blockloc.getBlockY() - 1, blockloc.getBlockZ()).getType() == Material.GOLD_BLOCK)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX() + 1, blockloc.getBlockY() - 1, blockloc.getBlockZ() - 1).getType() == Material.GOLD_BLOCK)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX() + 1, blockloc.getBlockY() - 1, blockloc.getBlockZ() + 1).getType() == Material.GOLD_BLOCK)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX(), blockloc.getBlockY() - 1, blockloc.getBlockZ() - 1).getType() == Material.GOLD_BLOCK)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX(), blockloc.getBlockY() - 1, blockloc.getBlockZ() + 1).getType() == Material.GOLD_BLOCK)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX(), blockloc.getBlockY(), blockloc.getBlockZ() + 1).getType() == Material.REDSTONE_TORCH_ON)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX(), blockloc.getBlockY(), blockloc.getBlockZ() - 1).getType() == Material.REDSTONE_TORCH_ON)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX() + 1, blockloc.getBlockY(), blockloc.getBlockZ()).getType() == Material.REDSTONE_TORCH_ON)
						&& (block.getWorld().getBlockAt(blockloc.getBlockX() - 1, blockloc.getBlockY(), blockloc.getBlockZ()).getType() == Material.REDSTONE_TORCH_ON)
						&& Herobrine.getPluginCore().getConfigDB().UseTotem && !AICore.isTotemCalled) {
					Herobrine.getPluginCore().getAICore().playerCallTotem(event.getPlayer());
				}
			}
		}
		if (event.getBlock().getWorld() == Bukkit.getServer().getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onBlockBreak(final BlockBreakEvent event) {
		if (event.getBlock().getWorld() == Bukkit.getServer().getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName)) {
			event.setCancelled(true);
			return;
		}
		final Heads h = (Heads) Herobrine.getPluginCore().getAICore().getCore(Core.CoreType.HEADS);
		final ArrayList<Block> list = h.getHeadList();
		if (list.contains(event.getBlock())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onBlockPlace(final BlockPlaceEvent event) {
		if (event.getBlock().getWorld() == Bukkit.getServer().getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName)) {
			event.setCancelled(true);
		}
	}

}