package net.theprogrammersworld.herobrine.nms.entity;

import net.theprogrammersworld.herobrine.Herobrine;

public class EntityInjector {

	public static void inject() {
		try {			
			CustomEntityRegistry.addCustomEntity(51, "Skeleton", CustomSkeleton.class);
			CustomEntityRegistry.addCustomEntity(54, "Zombie", CustomZombie.class);
		} catch (Throwable t) {
			t.printStackTrace();
			Herobrine.isNPCDisabled = true;
			Herobrine.log.warning("[Herobrine] Custom NPCs have been disabled due to a compatibility issue.");
		}
	}

}
