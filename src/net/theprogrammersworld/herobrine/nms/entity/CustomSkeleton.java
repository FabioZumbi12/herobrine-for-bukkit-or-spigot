package net.theprogrammersworld.herobrine.nms.entity;

import java.util.Random;

import net.minecraft.server.v1_12_R1.EntitySkeleton;
import net.minecraft.server.v1_12_R1.GenericAttributes;
import net.minecraft.server.v1_12_R1.World;
import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.misc.ItemName;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Skeleton;
import org.bukkit.inventory.ItemStack;

public class CustomSkeleton extends EntitySkeleton implements CustomEntity {

	private MobType mobType;

	public CustomSkeleton(final World world, final Location loc, final MobType mbt) {
		super(world);
		mobType = null;
		mobType = mbt;
		if (mbt == MobType.DEMON) {
			spawnDemon(loc);
		}
	}

	public void spawnDemon(final Location loc) {
		getAttributeInstance(GenericAttributes.c).setValue(Herobrine.getPluginCore().getConfigDB().npc.getDouble("npc.Demon.Speed"));
		getAttributeInstance(GenericAttributes.maxHealth).setValue(Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Demon.HP"));
		setHealth(Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Demon.HP"));
		setCustomName("Demon");
		((Skeleton) getBukkitEntity()).getEquipment().setItemInMainHand(new ItemStack(Material.GOLDEN_APPLE, 1));
		((Skeleton) getBukkitEntity()).getEquipment().setHelmet(ItemName.colorLeatherArmor(new ItemStack(Material.LEATHER_HELMET, 1), Color.RED));
		((Skeleton) getBukkitEntity()).getEquipment().setChestplate(ItemName.colorLeatherArmor(new ItemStack(Material.LEATHER_CHESTPLATE, 1), Color.RED));
		((Skeleton) getBukkitEntity()).getEquipment().setLeggings(ItemName.colorLeatherArmor(new ItemStack(Material.LEATHER_LEGGINGS, 1), Color.RED));
		((Skeleton) getBukkitEntity()).getEquipment().setBoots(ItemName.colorLeatherArmor(new ItemStack(Material.LEATHER_BOOTS, 1), Color.RED));
		getBukkitEntity().teleport(loc);
	}

	public CustomSkeleton(final World world) {
		super(world);
		mobType = null;
	}

	@Override
	@SuppressWarnings("deprecation")
	public void killCustom() {
		for (int i = 1; i <= 2500; ++i) {
			if (Herobrine.getPluginCore().getConfigDB().npc.contains("npc.Demon.Drops." + Integer.toString(i))) {
				final int chance = new Random().nextInt(100);
				if (chance <= Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Demon.Drops." + Integer.toString(i) + ".Chance")) {
					getBukkitEntity().getLocation().getWorld().dropItemNaturally(getBukkitEntity().getLocation(),
					new ItemStack(Material.getMaterial(i), Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Demon.Drops." + Integer.toString(i) + ".Count")));
				}
			}
		}
		setHealth(0.0f);
	}

	@Override
	public MobType getMobType() {
		return mobType;
	}

}
