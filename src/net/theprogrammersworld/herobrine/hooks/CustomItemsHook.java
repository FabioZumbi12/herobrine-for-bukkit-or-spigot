package net.theprogrammersworld.herobrine.hooks;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import net.theprogrammersworld.customitems.API;
import net.theprogrammersworld.customitems.CustomItems;

public class CustomItemsHook
{
  private CustomItems ci = null;
  private API api = null;
  
  public void init()
  {
    this.ci = ((CustomItems)Bukkit.getServer().getPluginManager().getPlugin("Custom_Items"));
    this.api = CustomItems.getAPI();
  }
  
  public boolean Check()
  {
    return Bukkit.getServer().getPluginManager().getPlugin("Custom_Items") != null;
  }
  
  public boolean checkItem(String name)
  {
    if (this.ci != null) {
      return this.api.itemExist(name);
    }
    return false;
  }
  
  public ItemStack getItem(String name)
  {
    return this.api.createItem(name);
  }
}
