package net.theprogrammersworld.herobrine.AI.cores;

import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.CoreResult;

import org.bukkit.entity.Player;

public class Burn extends Core {

	public Burn() {
		super(CoreType.BURN, AppearType.NORMAL);
	}

	@Override
	public CoreResult callCore(final Object[] data) {
		final Player player = (Player) data[0];
		player.setFireTicks(800);
		return new CoreResult(true, player.getDisplayName() + " was burned by Herobrine.");
	}

}