package net.theprogrammersworld.herobrine.AI.cores;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.CoreResult;

import org.bukkit.Bukkit;

public class RandomSound extends Core {

	public RandomSound() {
		super(CoreType.RANDOM_SOUND, AppearType.NORMAL);
	}

	@Override
	public CoreResult callCore(final Object[] data) {
		for (int multip = 1; multip != 7; ++multip) {
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Herobrine.getPluginCore(), new Runnable() {
				@Override
				public void run() {
					Herobrine.getPluginCore().getAICore().getCore(CoreType.SOUNDF).runCore(data);
				}
			}, multip * 30L);
		}
		return new CoreResult(true, "Herobrine haunted the target with sound.");
	}

}